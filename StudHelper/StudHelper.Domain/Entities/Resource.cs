﻿namespace StudHelper.Domain.Entities
{
    public class Resource
    {
        public int Id { get; set; }

        public int GroupId { get; set; }

        public Group Group { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}