﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace StudHelper.Domain.Entities
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IList<Group> CreatedGroups { get; set; }
    }
}