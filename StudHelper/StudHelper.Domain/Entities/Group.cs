﻿using System.Collections.Generic;

namespace StudHelper.Domain.Entities
{
    public class Group
    {
        public int Id { get; set; }

        public int OwnerId { get; set; }

        public User Owner { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public IList<Resource> Resources { get; set; }
    }
}