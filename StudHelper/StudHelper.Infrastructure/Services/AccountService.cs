﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.PasswordRecovery;
using StudHelper.Application.ViewModels.SignIn;
using StudHelper.Application.ViewModels.SignUp;
using StudHelper.Application.ViewModels.User;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Utils;

namespace StudHelper.Infrastructure.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IMapper mapper;

        public AccountService(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IMapper mapper)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.mapper = mapper;
        }

        public async Task<ResultViewModel> ConfirmEmailAsync(string userId, string token)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return new ResultViewModel()
                {
                    IsSucceeded = false,
                    Errors = ErrorExtension.GetErrorListFrom("User is not found")
                };
            }

            var result = await this.userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return new ResultViewModel() { IsSucceeded = true };
            }
            else
            {
                return new ResultViewModel()
                {
                    IsSucceeded = false,
                    Errors = ErrorExtension.GetErrorListFrom(result.Errors)
                };
            }
        }

        public async Task<ResultViewModel<UserTokenViewModel>> RequestPasswordRecoveryAsync(RequestPasswordRecoveryViewModel model)
        {
            var user = await this.userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return new ResultViewModel<UserTokenViewModel>()
                {
                    IsSucceeded = false,
                    Errors = ErrorExtension.GetErrorListFrom("User is not found")
                };
            }

            if (await this.userManager.IsEmailConfirmedAsync(user))
            {
                return new ResultViewModel<UserTokenViewModel>()
                {
                    IsSucceeded = false,
                    Errors = ErrorExtension.GetErrorListFrom("Email not confirmed")
                };
            }

            var token = await this.userManager.GeneratePasswordResetTokenAsync(user);

            return new ResultViewModel<UserTokenViewModel>() { Result = new UserTokenViewModel(user.Id, token), IsSucceeded = true };
        }

        public async Task<ResultViewModel> ResetPasswordAsync(PasswordRecoveryViewModel model)
        {
            var user = await this.userManager.FindByIdAsync(model.UserId.ToString());
            if (user == null)
            {
                return new ResultViewModel()
                {
                    IsSucceeded = false,
                    Errors = ErrorExtension.GetErrorListFrom("User is not found")
                };
            }

            var result = await this.userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (result.Succeeded)
            {
                return new ResultViewModel() { IsSucceeded = true };
            }

            return new ResultViewModel()
            {
                IsSucceeded = false,
                Errors = ErrorExtension.GetErrorListFrom(result.Errors)
            };
        }

        public async Task<ResultViewModel> SignIn(SignInViewModel model)
        {
            var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, model.StaySignedIn, false);
            if (result.Succeeded)
            {
                return new ResultViewModel<UserViewModel>() { IsSucceeded = true };
            }
            else
            {
                return new ResultViewModel() { Errors = new List<string>() { "Incorrect email or password" } };
            }
        }

        public async Task<ResultViewModel<UserTokenViewModel>> SignUp(SignUpViewModel model)
        {
            User newUser = this.mapper.Map<User>(model);

            var result = await this.userManager.CreateAsync(newUser, model.Password);

            if (result.Succeeded)
            {
                var token = await this.userManager.GenerateEmailConfirmationTokenAsync(newUser);
                return new ResultViewModel<UserTokenViewModel>()
                {
                    Result = new UserTokenViewModel(newUser.Id, token),
                    IsSucceeded = true
                };
            }
            else
            {
                return new ResultViewModel<UserTokenViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom(result.Errors) };
            }
        }
    }
}