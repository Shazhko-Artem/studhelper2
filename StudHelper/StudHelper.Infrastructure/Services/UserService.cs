﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Settings;
using StudHelper.Application.ViewModels.User;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Utils;

namespace StudHelper.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public UserService(UserManager<User> userManager, IMapper mapper)
        {
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<ResultViewModel<UserViewModel>> GetUserAsync(Expression<Func<User, bool>> filter)
        {
            var foundUser = await this.userManager.Users.FirstOrDefaultAsync(filter);

            if (foundUser == null)
            {
                return new ResultViewModel<UserViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("User is not found") };
            }

            return new ResultViewModel<UserViewModel>() { IsSucceeded = true, Result = this.mapper.Map<UserViewModel>(foundUser) };
        }

        public async Task<ResultViewModel<UserViewModel>> FindByNameAsync(string name)
        {
            var foundUser = await this.userManager.FindByNameAsync(name);

            if (foundUser == null)
            {
                return new ResultViewModel<UserViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("User is not found") };
            }

            return new ResultViewModel<UserViewModel>() { IsSucceeded = true, Result = this.mapper.Map<UserViewModel>(foundUser) };
        }

        public async Task<ResultViewModel<IList<UserViewModel>>> GetUsersAsync(Expression<Func<User, bool>> filter)
        {
            var foundUsers = await this.userManager.Users.Where(filter).ToListAsync();

            if (foundUsers == null)
            {
                return new ResultViewModel<IList<UserViewModel>>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("User is not found") };
            }

            return new ResultViewModel<IList<UserViewModel>>() { IsSucceeded = true, Result = this.mapper.Map<List<UserViewModel>>(foundUsers) };
        }

        public async Task<ResultViewModel> ChangePasswordAsync(SettingPasswordViewModel settingPassword)
        {
            var foundUser = await this.userManager.Users.FirstOrDefaultAsync(u => u.Id == settingPassword.UserId);

            if (foundUser == null)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("User is not found") };
            }

            var result = await this.userManager.ChangePasswordAsync(foundUser, settingPassword.OldPassword, settingPassword.Password);

            if (!result.Succeeded)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom(result.Errors) };
            }

            return new ResultViewModel() { IsSucceeded = true };
        }

        public async Task<ResultViewModel> UpdateAsync(int currentUserId, SettingProfileViewModel settingProfile)
        {
            if (currentUserId != settingProfile.UserId)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Access denied") };
            }

            var currentUser = await this.userManager.Users.FirstOrDefaultAsync(u => u.Id == currentUserId);

            if (currentUser == null)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("User is not found") };
            }

            this.mapper.Map(settingProfile, currentUser);

            var result = await this.userManager.UpdateAsync(currentUser);

            if (!result.Succeeded)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom(result.Errors) };
            }

            return new ResultViewModel() { IsSucceeded = true };
        }
    }
}