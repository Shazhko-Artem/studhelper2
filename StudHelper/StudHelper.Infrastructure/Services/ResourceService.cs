﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using StudHelper.Application.Interfaces;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Utils;

namespace StudHelper.Infrastructure.Services
{
    public class ResourceService : IResourceService
    {
        private readonly IRepository<Resource> repository;
        private readonly IMapper mapper;

        public ResourceService(IRepository<Resource> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<ResultViewModel> AddAsync(int userId, ResourceViewModel resource)
        {
            int resourceId = resource.Id;
            var owner = await this.repository.GetFirstAsync<OwnerIdViewModel>(r => r.Id == resourceId);

            if (owner.OwnerId != userId)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Access denied") };
            }

            Resource newResource = this.mapper.Map<Resource>(resource);
            if (await this.repository.AddAsync(newResource))
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Add resource error") };
            }

            if (await this.repository.SaveChangesAsync())
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Failed to save changes") };
            }

            return new ResultViewModel() { IsSucceeded = true };
        }
    }
}