﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using StudHelper.Application.Interfaces;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.Group;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Utils;

namespace StudHelper.Infrastructure.Services
{
    public class GroupService : IGroupService
    {
        private readonly IRepository<Group> repository;
        private readonly IMapper mapper;

        public GroupService(IRepository<Group> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<ResultViewModel> AddAsync(CreateGroupViewModel group)
        {
            Group newGroup = this.mapper.Map<Group>(group);

            await this.repository.AddAsync(newGroup);
            if (!await this.repository.SaveChangesAsync())
            {
                return new ResultViewModel<GroupViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Failed to save changes") };
            }

            group.Id = newGroup.Id;
            return new ResultViewModel() { IsSucceeded = true };
        }

        public Task<int> CountAsync()
        {
            return this.repository.CountAsync();
        }

        public Task<IList<GroupListItemViewModel>> GetAllAsync(int offset = 0, int size = -1)
        {
            return this.repository.GetAsync<GroupListItemViewModel>(offset: offset, size: size);
        }

        public Task<GroupViewModel> GetAsync(int id)
        {
            return this.repository.GetFirstAsync<GroupViewModel>(g => g.Id == id);
        }

        public async Task<ResultViewModel<UpdateGroupViewModel>> GetToUpdateAsync(int currentUserId, int groupId)
        {
            var foundGroup = await this.repository.GetFirstAsync<GroupViewModel>(g => g.Id == groupId);
            if (foundGroup == null)
            {
                return new ResultViewModel<UpdateGroupViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Group is not found") };
            }

            if (foundGroup.Owner.Id != currentUserId)
            {
                return new ResultViewModel<UpdateGroupViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Access denied") };
            }

            var viewModel = this.mapper.Map<UpdateGroupViewModel>(foundGroup);
            return new ResultViewModel<UpdateGroupViewModel>() { IsSucceeded = true, Result = viewModel };
        }

        public async Task<ResultViewModel> UpdateAsync(int currentUserId, UpdateGroupViewModel viewModel)
        {
            var originGroup = await this.repository.GetFirstAsync<OwnerIdViewModel>(u => u.Id == viewModel.Id);
            if (originGroup == null)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Group is not found") };
            }

            if (originGroup.OwnerId != currentUserId)
            {
                return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Access denied") };
            }

            if (viewModel.IsModified)
            {
                Group newGroup = this.mapper.Map<Group>(viewModel);
                newGroup.OwnerId = originGroup.OwnerId;
                if (!await this.repository.UpdateAsync(newGroup))
                {
                    return new ResultViewModel() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Update data error") };
                }

                if (!await this.repository.SaveChangesAsync())
                {
                    return new ResultViewModel<GroupViewModel>() { IsSucceeded = false, Errors = ErrorExtension.GetErrorListFrom("Failed to save changes") };
                }
            }

            return new ResultViewModel() { IsSucceeded = true } ;
        }
    }
}