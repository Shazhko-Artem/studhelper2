﻿using System.Collections.Generic;
using StudHelper.Application.Interfaces.Notifications;

namespace StudHelper.Infrastructure.Notifications
{
    public class MessageProvider : IMessageProvider
    {
        private readonly Dictionary<EmailMessageType, string> messages = new Dictionary<EmailMessageType, string>()
        {
            { EmailMessageType.EmailConfirmation, "Please confirm your account by <a href='{0}'>clicking here</a>." },
            { EmailMessageType.PasswordRecovery,"To recover your password, <a href='{0}'>clicking here</a>." }
        };

        public string GetMessage(EmailMessageType type, string callBackUrl)
        {
            return string.Format(this.messages[type], callBackUrl);
        }
    }
}