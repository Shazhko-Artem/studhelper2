﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;

namespace StudHelper.Infrastructure.Notifications
{
    public class EmailSenderStub : IEmailSender
    {
        private readonly ILogger<EmailSenderStub> logger;

        public EmailSenderStub(ILogger<EmailSenderStub> logger)
        {
            this.logger = logger;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            this.logger.LogInformation(
                $"email: {email}{Environment.NewLine}" +
                $"subject: {subject}{Environment.NewLine}" +
                $"message: {htmlMessage}");

            return Task.CompletedTask;
        }
    }
}