﻿using AutoMapper;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.Group;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Application.ViewModels.Settings;
using StudHelper.Application.ViewModels.SignUp;
using StudHelper.Application.ViewModels.User;
using StudHelper.Domain.Entities;

namespace StudHelper.Infrastructure.Utils
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            this.CreateGroupMap();
            this.CreateUserMap();
            this.CreateResourceMap();
        }

        private void CreateGroupMap()
        {
            this.CreateMap<Group, GroupViewModel>();
            this.CreateMap<Group, GroupListItemViewModel>()
                .ForMember(group => group.OwnerId, opt => opt.MapFrom(src => src.Owner.Id))
                .ForMember(group => group.OwnerName, opt => opt.MapFrom(src => src.Owner.UserName));
            this.CreateMap<Group, CreateGroupViewModel>();
            this.CreateMap<GroupViewModel, UpdateGroupViewModel>()
                .ForMember(group => group.IsModified, opt => opt.Ignore());
            this.CreateMap<UpdateGroupViewModel, Group>()
               .ForMember(group => group.Owner, opt => opt.Ignore())
               .ForMember(group => group.OwnerId, opt => opt.Ignore());
            this.CreateMap<CreateGroupViewModel, Group>()
                .ForMember(group => group.Owner, opt => opt.Ignore());
        }

        private void CreateResourceMap()
        {
            this.CreateMap<Resource, ResourceViewModel>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            this.CreateMap<ResourceViewModel, Resource>()
                .ForMember(resource => resource.GroupId, opt => opt.Ignore())
                .ForMember(resource => resource.Group, opt => opt.Ignore());
            this.CreateMap<Resource, OwnerIdViewModel>()
                .ForMember(des => des.OwnerId, opt => opt.MapFrom(src => src.Group.OwnerId));
        }

        private void CreateUserMap()
        {
            this.CreateMap<User, UserViewModel>();
            this.CreateMap<SettingProfileViewModel, User>();
            this.CreateMap<SignUpViewModel, User>().ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
        }
    }
}