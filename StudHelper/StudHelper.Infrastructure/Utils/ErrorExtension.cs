﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace StudHelper.Infrastructure.Utils
{
    public static class ErrorExtension
    {
        public static List<string> GetErrorListFrom(IEnumerable<IdentityError> identityErrors)
        {
            List<string> listErrors = new List<string>();

            foreach (var error in identityErrors)
            {
                listErrors.Add(error.Description);
            }

            return listErrors;
        }

        public static List<string> GetErrorListFrom(params string[] errorDescription)
        {
            List<string> listErrors = new List<string>();

            foreach (var error in errorDescription)
            {
                if (!string.IsNullOrEmpty(error))
                {
                    listErrors.Add(error);
                }
            }

            return listErrors;
        }
    }
}