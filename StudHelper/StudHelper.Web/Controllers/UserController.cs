﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.Interfaces.Services;

namespace StudHelper.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<IActionResult> Profile(int id)
        {
            var model = await this.userService.GetUserAsync(u => u.Id == id);

            if (!model.IsSucceeded)
            {
                return this.StatusCode(500);
            }

            return this.View(model.Result);
        }
    }
}