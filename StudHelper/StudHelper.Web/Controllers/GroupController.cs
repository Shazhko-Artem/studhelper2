﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.Group;
using StudHelper.Web.Utils;

namespace StudHelper.Web.Controllers
{
    [Authorize]
    public class GroupController : Controller
    {
        private const int PageSize = 4;
        private const int MaxPage = 4;
        private readonly IGroupService groupService;

        public GroupController(IGroupService groupService)
        {
            this.groupService = groupService;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Info(int id)
        {
           var model = await this.groupService.GetAsync(id);
            if (model == null)
            {
                return this.NotFound();
            }

            return this.View(model);
        }

        [AllowAnonymous]
        public async Task<IActionResult> List(int page = 1)
        {
            int offset = (page - 1) * PageSize;
            var groupListItems = await this.groupService.GetAllAsync(offset, PageSize);
            int totalSize = await this.groupService.CountAsync();
            var viewModel = new GroupListViewModel()
            {
                Groups = groupListItems,
                Pager = new JW.Pager(totalSize, page, PageSize, MaxPage)
            };

            return this.View(viewModel);
        }

        public IActionResult Create()
        {
            var viewModel = new CreateGroupViewModel();
            return this.View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateGroupViewModel viewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            viewModel.OwnerId = this.User.GetUserId();
            var addResult = await this.groupService.AddAsync(viewModel);

            if (!addResult.IsSucceeded)
            {
                return this.StatusCode(500);
            }

            return this.RedirectToAction("Info", new { id=viewModel.Id });
        }

        public async Task<IActionResult> Update(int id)
        {
            int userId = this.User.GetUserId();
            var viewModelResult = await this.groupService.GetToUpdateAsync(userId, id);
            if (!viewModelResult.IsSucceeded)
            {
                return this.NotFound();
            }

            return this.View(viewModelResult.Result);
        }

        [HttpPost]
        public async Task<IActionResult> Update(UpdateGroupViewModel viewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            int userId = this.User.GetUserId();
            var addResult = await this.groupService.UpdateAsync(userId, viewModel);

            if (!addResult.IsSucceeded)
            {
                return this.StatusCode(500);
            }

            return this.RedirectToAction("Info", new { id = viewModel.Id });
        }
    }
}