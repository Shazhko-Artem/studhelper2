﻿using System.ComponentModel.Design;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Web.Utils;

namespace StudHelper.Web.Controllers
{
    [Authorize]
    public class ResourceController : Controller
    {
        private readonly IResourceService service;

        public ResourceController(IResourceService service)
        {
            this.service = service;
        }

        public IActionResult Create(int groupId)
        {
            return this.View(new ResourceViewModel() { GroupId = groupId });
        }

        [HttpPost]
        public async Task<IActionResult> Create(ResourceViewModel viewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            int userId = this.User.GetUserId();
            var addresult = await this.service.AddAsync(userId, viewModel);
            
            return this.View(new ResourceViewModel() { GroupId = groupId });
        }
    }
}