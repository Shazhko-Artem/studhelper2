﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudHelper.Application.Interfaces;
using StudHelper.Application.Interfaces.Notifications;
using StudHelper.Application.Interfaces.Services;
using StudHelper.Application.ViewModels.SignUp;
using StudHelper.Domain.Entities;
using StudHelper.Infrastructure.Notifications;
using StudHelper.Infrastructure.Services;
using StudHelper.Persistence;
using StudHelper.Persistence.Data;

namespace StudHelper.Web
{

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<StudHelperDbContext>(
                options => options.UseSqlServer(this.Configuration.GetConnectionString("StudHelperDatabase")));

            services.AddIdentity<User, IdentityRole<int>>()
               .AddEntityFrameworkStores<StudHelperDbContext>()
                               .AddDefaultTokenProviders();

            services.AddAutoMapper();

            services.AddTransient<IEmailSender, EmailSenderStub>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRepository<Group>, SqlRepository<Group>>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<IMessageProvider, MessageProvider>();
            services.Configure<AuthMessageSenderOptions>(this.Configuration);
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Account/SignIn";
            });
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddMvc()
                    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<SignUpViewModelValidator>())
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: null,
                    template: "Settings/{action=Profile}",
                    defaults: new { controller = "Settings" });
                routes.MapRoute(
                  name: null,
                  template: "User/{action=Profile}/{id?}",
                  defaults: new { controller = "User" });
                routes.MapRoute(
                    name: null,
                    template: "{controller=Group}/{action=List}/page{page}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Group}/{action=List}/{id?}");
            });
        }
    }
}