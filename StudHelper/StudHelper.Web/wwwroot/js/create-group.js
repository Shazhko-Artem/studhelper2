﻿;
var groupViewModel = (function () {
    var modalState = "add";
    var currentResource;
    const resourceModal = $('[modal="resource"]');
    const resourceModalTitle = resourceModal.find("[resource-title]");
    const resourceModalDescription = resourceModal.find("[resource-description]");
    const resourceList = $("#resource_list");


    function ViewModel() {
        this.onBtnResourceDeleteClick = function (self) {
            const button = $(self);
            const resource = button.closest("[resources-item]");
            resource.remove();
            resourceIndexing(resourceList);
        }

        this.showResourceModal = function (self) {
            resourceModal.modal("show");
            const button = $(self);
            modalState = button.data("mode");

            if (modalState === "edit") {
                currentResource = button.closest("[resources-item]");
                const title = currentResource.find("[resource-title]").html();
                const description = currentResource.find("[resource-description]").html();
                resourceModalTitle.val(title);
                resourceModalDescription.val(description);
            } else {
                resourceModalTitle.val("");
                resourceModalDescription.val("");
            }
        }
    }
    var viewModelInstance = new ViewModel();

    resourceModal.find("[action-apply]").click(function (self) {
        if (!$(this).closest("form").valid()) {
            return;
        }

        const title = resourceModalTitle.val();
        const description = resourceModalDescription.val();

        if (modalState === "add") {
            addNewResource(title, description);
        } else {
            updateResource(title, description);
        }

        resourceIndexing();

        resourceModal.modal("hide");
    });

    function addNewResource(title, description) {
        const newResourcesItem = buildResourceListItem(title, description);
        resourceList.append(newResourcesItem);
        $("#resourceErrorMsg").remove();
    }


    function resourceIndexing() {
        const resourceTitleNameTemplate = resourceList.data("resource-title-name-template");
        const resourceDescNameTemplate = resourceList.data("resource-description-name-template");

        resourceList.find("[resources-item]").each(function (index, item) {

            const resourceTitle = $(item).find("[resource-title]");
            const resourceDesc = $(item).find("[resource-description]");

            const titleName = resourceTitleNameTemplate.replace("{index}", index);
            const descriptionName = resourceDescNameTemplate.replace("{index}", index);
            $(resourceTitle).attr("name", titleName);
            $(resourceDesc).attr("name", descriptionName);
        });
    }

    function updateResource(title, description) {
        currentResource.find("[resource-title]").each(function (index, item) {
            const element = $(item);
            if (element[0].localName === "input") {
                element.attr("value", title);
            } else {
                element.html(title);
            }
        });
        currentResource.find("[resource-description]").each(function (index, item) {
            const element = $(item);
            if (element[0].localName === "input") {
                element.attr("value", description);
            } else {
                element.html(description);
            }
        });
    }

    function buildResourceListItem(title, description) {
        const resourceContextMap = {
            '{title}': title,
            '{description}': description
        }
        const resourceStr = $("#resourceTemplate").html().replace(/{title}|{description}/gi, function (matched) {
            return resourceContextMap[matched];
        });

        return $(resourceStr);
    }

    return viewModelInstance;
})();