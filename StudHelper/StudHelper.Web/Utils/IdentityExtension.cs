﻿using System.Security.Claims;

namespace StudHelper.Web.Utils
{
    public static class IdentityExtension
    {
        public static int GetUserId(this ClaimsPrincipal claims)
        {
            if (!claims.Identity.IsAuthenticated)
            {
                return -1;
            }

            var strUserId = claims.FindFirstValue(ClaimTypes.NameIdentifier);
            return int.Parse(strUserId);
        }
    }
}