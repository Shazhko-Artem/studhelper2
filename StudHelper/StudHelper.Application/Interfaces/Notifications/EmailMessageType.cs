﻿namespace StudHelper.Application.Interfaces.Notifications
{
    public enum EmailMessageType
    {
        EmailConfirmation,
        PasswordRecovery
    }
}