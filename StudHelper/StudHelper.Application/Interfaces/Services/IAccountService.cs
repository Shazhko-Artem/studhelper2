﻿using System.Threading.Tasks;
using StudHelper.Application.ViewModels.Entities;
using StudHelper.Application.ViewModels.PasswordRecovery;
using StudHelper.Application.ViewModels.SignIn;
using StudHelper.Application.ViewModels.SignUp;
using StudHelper.Application.ViewModels.Utils;

namespace StudHelper.Application.Interfaces.Services
{
    public interface IAccountService
    {
        Task<ResultViewModel> SignIn(SignInViewModel model);

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="model">New user description</param>
        /// <returns>Token to confirm email</returns>
        Task<ResultViewModel<UserTokenViewModel>> SignUp(SignUpViewModel model);

        Task<ResultViewModel> ConfirmEmailAsync(string userId, string token);

        Task<ResultViewModel<UserTokenViewModel>> RequestPasswordRecoveryAsync(RequestPasswordRecoveryViewModel model);

        Task<ResultViewModel> ResetPasswordAsync(PasswordRecoveryViewModel model);
    }
}