﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using StudHelper.Application.ViewModels.Settings;
using StudHelper.Application.ViewModels.User;
using StudHelper.Application.ViewModels.Utils;
using StudHelper.Domain.Entities;

namespace StudHelper.Application.Interfaces.Services
{
    public interface IUserService
    {
        Task<ResultViewModel<UserViewModel>> GetUserAsync(Expression<Func<User, bool>> filter);

        Task<ResultViewModel<IList<UserViewModel>>> GetUsersAsync(Expression<Func<User, bool>> filter);

        Task<ResultViewModel<UserViewModel>> FindByNameAsync(string name);

        Task<ResultViewModel> ChangePasswordAsync(SettingPasswordViewModel settingPassword);

        Task<ResultViewModel> UpdateAsync(int currentUserId, SettingProfileViewModel user);
    }
}