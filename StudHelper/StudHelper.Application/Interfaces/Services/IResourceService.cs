﻿using System.Threading.Tasks;
using StudHelper.Application.ViewModels.Resource;
using StudHelper.Application.ViewModels.Utils;

namespace StudHelper.Application.Interfaces.Services
{
    public interface IResourceService
    {
        Task<ResultViewModel> AddAsync(int userId, ResourceViewModel resource);
    }
}