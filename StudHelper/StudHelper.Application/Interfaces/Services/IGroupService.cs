﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudHelper.Application.ViewModels.Group;
using StudHelper.Application.ViewModels.Utils;

namespace StudHelper.Application.Interfaces.Services
{
    public interface IGroupService
    {
        Task<ResultViewModel> AddAsync(CreateGroupViewModel group);

        Task<IList<GroupListItemViewModel>> GetAllAsync(int offset = 0, int size = -1);

        Task<GroupViewModel> GetAsync(int id);

        Task<ResultViewModel<UpdateGroupViewModel>> GetToUpdateAsync(int currentUserId, int groupId);

        Task<ResultViewModel> UpdateAsync(int currentUserId, UpdateGroupViewModel viewModel);

        Task<int> CountAsync();
    }
}