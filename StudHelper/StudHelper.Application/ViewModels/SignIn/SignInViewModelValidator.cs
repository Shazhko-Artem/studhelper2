﻿using FluentValidation;
using StudHelper.Application.Resources;

namespace StudHelper.Application.ViewModels.SignIn
{
    public class SignInViewModelValidator : AbstractValidator<SignInViewModel>
    {
        public SignInViewModelValidator()
        {
            this.RuleFor(x => x.Email).EmailAddress().WithMessage(ValidationMessages.WrongEmailFormat);
            this.RuleFor(x => x.Password).NotEmpty();
        }
    }
}