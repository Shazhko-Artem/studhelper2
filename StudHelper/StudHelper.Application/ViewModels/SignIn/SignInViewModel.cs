﻿namespace StudHelper.Application.ViewModels.SignIn
{
    public class SignInViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public bool StaySignedIn { get; set; }

        public string ReturnUrl { get; set; }
    }
}