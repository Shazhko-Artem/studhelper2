﻿using StudHelper.Application.ViewModels.User;
using System.ComponentModel.DataAnnotations;

namespace StudHelper.Application.ViewModels.Settings
{
    public class SettingPasswordViewModel
    {
        public SettingPasswordViewModel()
        {
        }

        public SettingPasswordViewModel(UserViewModel userViewModel)
        {
            this.UserId = userViewModel.Id;
        }

        public int UserId { get; set; }

        [Display(Name = "Old password")]
        public string OldPassword { get; set; }

        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }

        public bool Done { get; set; }
    }
}