﻿using StudHelper.Application.ViewModels.User;
using System.ComponentModel.DataAnnotations;

namespace StudHelper.Application.ViewModels.Settings
{
    public class SettingProfileViewModel
    {
        public SettingProfileViewModel()
        {
        }

        public SettingProfileViewModel(UserViewModel userViewModel)
        {
            this.UserId = userViewModel.Id;
            this.FirstName = userViewModel.FirstName;
            this.LastName = userViewModel.LastName;
        }

        public int UserId { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public bool Done { get; set; }
    }
}