﻿using FluentValidation;
using StudHelper.Application.Resources;

namespace StudHelper.Application.ViewModels.Settings
{
    public class SettingProfileViewModelValidator : AbstractValidator<SettingProfileViewModel>
    {
        public SettingProfileViewModelValidator()
        {
            this.RuleFor(x => x.FirstName).NotEmpty().WithName(ValidationMessages.FirstName);
            this.RuleFor(x => x.LastName).NotEmpty().WithName(ValidationMessages.LastName);
        }
    }
}