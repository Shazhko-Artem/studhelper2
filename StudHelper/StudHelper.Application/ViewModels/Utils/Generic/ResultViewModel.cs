﻿namespace StudHelper.Application.ViewModels.Utils
{
    public class ResultViewModel<T> : ResultViewModel
        where T : class
    {
        public T Result { get; set; }
    }
}