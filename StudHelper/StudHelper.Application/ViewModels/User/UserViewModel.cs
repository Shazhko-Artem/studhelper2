﻿using System.ComponentModel.DataAnnotations;

namespace StudHelper.Application.ViewModels.User
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }
    }
}