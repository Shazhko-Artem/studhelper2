﻿using FluentValidation;

namespace StudHelper.Application.ViewModels.Resource
{
    public class ResourceViewModelValidator : AbstractValidator<ResourceViewModel>
    {
        public ResourceViewModelValidator()
        {
            this.RuleFor(x => x.Title).NotNull().NotEmpty().MaximumLength(60).MinimumLength(2);
            this.RuleFor(x => x.Description).NotNull().NotEmpty().MaximumLength(5120);
        }
    }
}