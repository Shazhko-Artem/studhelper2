﻿namespace StudHelper.Application.ViewModels.Resource
{
    public class ResourceViewModel
    {
        public int Id { get; set; }

        public int GroupId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}