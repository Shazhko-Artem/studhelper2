﻿using System.Collections.Generic;
using StudHelper.Application.ViewModels.Resource;

namespace StudHelper.Application.ViewModels.Group
{
    public class UpdateGroupViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsModified { get; set; }

        public IList<UpdateResourceViewModel> Resources { get; set; }
    }
}