﻿namespace StudHelper.Application.ViewModels.Group
{
    public class GroupListItemViewModel
    {
        private const int MaxDescriptionlength = 1024;
        private string description;

        public int Id { get; set; }

        public string Title { get; set; }

        public int OwnerId { get; set; }

        public string OwnerName { get; set; }

        public string Description
        {
            get
            {
                return this.description;
            }

            set
            {
                if (value == null)
                {
                    this.description = string.Empty;
                    return;
                }

                if (value.Length > MaxDescriptionlength)
                {
                    this.description = value.Substring(0, MaxDescriptionlength);
                    return;
                }

                this.description = value;
            }
        }
    }
}