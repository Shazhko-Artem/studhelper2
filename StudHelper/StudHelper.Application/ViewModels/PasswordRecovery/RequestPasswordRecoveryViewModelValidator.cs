﻿using FluentValidation;

namespace StudHelper.Application.ViewModels.PasswordRecovery
{
    public class RequestPasswordRecoveryViewModelValidator : AbstractValidator<RequestPasswordRecoveryViewModel>
    {
        public RequestPasswordRecoveryViewModelValidator()
        {
            // TODO: move all strings to resources
            this.RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Please enter a valid email");
        }
    }
}