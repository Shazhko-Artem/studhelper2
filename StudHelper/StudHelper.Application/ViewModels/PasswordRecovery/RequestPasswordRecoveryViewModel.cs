﻿namespace StudHelper.Application.ViewModels.PasswordRecovery
{
    public class RequestPasswordRecoveryViewModel
    {
        public string Email { get; set; }
    }
}