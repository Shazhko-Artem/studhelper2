﻿using FluentValidation;

namespace StudHelper.Application.ViewModels.PasswordRecovery
{
    public class PasswordRecoveryViewModelValidator : AbstractValidator<PasswordRecoveryViewModel>
    {
        public PasswordRecoveryViewModelValidator()
        {
            this.RuleFor(x => x.Password).NotEmpty();
            this.RuleFor(x => x.ConfirmPassword).Equal(x => x.Password);
        }
    }
}