﻿using System.ComponentModel.DataAnnotations;

namespace StudHelper.Application.ViewModels.PasswordRecovery
{
    public class PasswordRecoveryViewModel
    {
        public PasswordRecoveryViewModel()
        {

        }

        public PasswordRecoveryViewModel(int userId, string token)
        {
            this.Token = token;
            this.UserId = userId;
        }

        public string Token { get; set; }

        public int UserId { get; set; }

        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }
}