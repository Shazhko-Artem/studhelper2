﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ReflectionMap_task03.SimpleMapper
{
    public class SimpleMapper : IMapper
    {
        public TOut Map<TOut>(object source)
        {
            Type definitionType = typeof(TOut);
            Type sourcetype = source.GetType();
            TOut result = Activator.CreateInstance<TOut>();
            this.MapProperties(result, definitionType, source, sourcetype);
            this.MapFields(result, definitionType, source, sourcetype);
            return result;
        }

        private void MapProperties<TEntity>(TEntity entity, Type entityType, object source, Type sourceType)
        {
            List<PropertyInfo> entityTypeProperties = entityType.GetProperties().ToList();
            foreach (var sourceProperty in sourceType.GetProperties()
                .Where(info => info.GetMethod != null ? info.GetMethod.IsPublic : false))
            {
                PropertyInfo foundProperty = entityTypeProperties
                    .FirstOrDefault(info => info.SetMethod != null ? info.SetMethod.IsPublic : false && info.Name == sourceProperty.Name
                        && info.PropertyType.IsAssignableFrom(sourceProperty.PropertyType));

                if (foundProperty != null)
                {
                    foundProperty.SetValue(entity, sourceProperty.GetValue(source));
                    entityTypeProperties.Remove(foundProperty);
                }
            }
        }

        private void MapFields<TEntity>(TEntity entity, Type entityType, object source, Type sourceType)
        {
            List<FieldInfo> entityTypeFields = entityType.GetFields().ToList();
            foreach (var sourceField in sourceType.GetFields()
                .Where(info => info.IsPublic))
            {
                FieldInfo foundField = entityTypeFields
                    .FirstOrDefault(info => info.IsPublic && info.Name == sourceField.Name
                        && info.FieldType.IsAssignableFrom(sourceField.FieldType));

                if (foundField != null)
                {
                    foundField.SetValue(entity, sourceField.GetValue(source));
                    entityTypeFields.Remove(foundField);
                }
            }
        }
    }
}