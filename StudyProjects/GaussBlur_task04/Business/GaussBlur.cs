﻿using System.Drawing;
using System.Threading.Tasks;

namespace GaussBlur_task04.Business
{
    public sealed class GaussBlur
    {
        private Color[][] pixels;
        private int height;
        private int width;

        public GaussBlur(Color[][] pixels, int width, int height)
        {
            this.pixels = pixels;
            this.height = height;
            this.width = width;
        }

        public async Task BlurImage(bool isParallel = false)
        {
            int firstPartY = this.height / 2;

            if (isParallel)
            {
                await Task.WhenAll(
                Task.Run(() => this.BlurPartImage(0, 0, this.width, firstPartY)),
                Task.Run(() => this.BlurPartImage(0, firstPartY, this.height, this.width)));
            }
            else
            {
                this.BlurPartImage(0, 0, this.height, this.width);
            }
        }

        private void BlurPartImage(int startX, int startY, int endX, int endY)
        {
            for (int y = startY; y < endY; y++)
            {
                for (int x = startX; x < endX; x++)
                {
                    int addX = 0, addY = 0;
                    int offsetX = x, offsetY = y;
                    if (y == 0)
                    {
                        offsetY += 1;
                    }
                    else
                    {
                        offsetY -= 1;
                        if (y < endY - 1)
                        {
                            addY += 1;
                        }
                    }

                    if (x == 0)
                    {
                        offsetX += 1;
                    }
                    else
                    {
                        offsetX -= 1;
                        if (x < endX - 1)
                        {
                            addX += 1;
                        }
                    }

                    this.pixels[y][x] = this.AveragePixelsValue(offsetX, offsetY, x + addX, y + addY);
                }
            }
        }

        private Color AveragePixelsValue(int startX, int startY, int endX, int endY)
        {
            int r = 0, g = 0, b = 0;
            if (startX > endX)
            {
                int tempX = startX;
                startX = endX;
                endX = tempX;
            }

            if (startY > endY)
            {
                int tempY = startY;
                startY = endY;
                endY = tempY;
            }

            for (int y = startY; y <= endY; y++)
            {
                for (int x = startX; x <= endX; x++)
                {
                    Color currentColor = this.pixels[y][x];
                    r += currentColor.R;
                    g += currentColor.G;
                    b += currentColor.B;
                }
            }

            int width = (endX - startX) + 1;
            int height = (endY - startY) + 1;
            int size = width * height;

            r = (byte)(r / size);
            g = (byte)(g / size);
            b = (byte)(b / size);

            return Color.FromArgb(0, r, g, b);
        }
    }
}