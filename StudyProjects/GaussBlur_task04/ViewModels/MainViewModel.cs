﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GaussBlur_task04.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private BitmapImage originBitmapImage;
        private BitmapImage blurredBitmapImage;
        private bool isNotRunProcessing;

        public Color[][] ImagePixels { get; set; }

        private long speedTime;

        private bool isParallel;

        public ICommand BlurImageCommand { get; set; }

        public ICommand LoadImageCommand { get; set; }

        public string ImagePath { get; set; }

        public BitmapImage OriginBitmapImage
        {
            get
            {
                return this.originBitmapImage;
            }

            set
            {
                this.originBitmapImage = value;
                this.OnPropertyChanged();
            }
        }

        public BitmapImage BlurredBitmapImage
        {
            get
            {
                return this.blurredBitmapImage;
            }

            set
            {
                this.blurredBitmapImage = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsParallel
        {
            get
            {
                return this.isParallel;
            }

            set
            {
                this.isParallel = value;
                this.OnPropertyChanged();
            }
        }

        public long SpeedTime
        {
            get
            {
                return this.speedTime;
            }

            set
            {
                this.speedTime = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsNotRunProcessing
        {
            get
            {
                return this.isNotRunProcessing;
            }

            set
            {
                this.isNotRunProcessing = value;
                this.OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            this.IsNotRunProcessing = true;
            this.BlurImageCommand = new Commands.BlurImageCommand(this);
            this.LoadImageCommand = new Commands.LoadImageCommand(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}