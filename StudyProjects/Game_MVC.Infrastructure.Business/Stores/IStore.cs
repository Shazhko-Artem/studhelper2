﻿namespace Game_MVC.Infrastructure.Business.Stores
{
    public interface IStore<TEntity>
    {
        void Save();

        TEntity Load();
    }
}