﻿namespace Game_MVC.Infrastructure.Business
{
    using System;
    using System.Collections.Generic;
    using FluentValidation;
    using Game_MVC.Domain.Core;
    using Game_MVC.Domain.Interfaces;
    using Game_MVC.Services.Interfaces;

    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment> repository;
        private readonly AbstractValidator<Comment> validator;

        public CommentService(IRepository<Comment> repository, AbstractValidator<Comment> validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        public OperationResult<Comment> Add(string userName, string message)
        {
            Comment newComment = new Comment(userName, message);
            var validationResult = this.validator.Validate(newComment);
            if (!validationResult.IsValid)
            {
                return new OperationResult<Comment>() { Message = "Comment is not valid",  Code = ErrorCode.ItemIsNotValid };
            }

            var comment = this.repository.Add(newComment);

            if (!this.repository.SaveChanges())
            {
                return new OperationResult<Comment>() { Message = "Could not add comment", Code = ErrorCode.Other };
            }

            return new OperationResult<Comment>() { Result = comment };
        }

        public OperationResult<IList<Comment>> Get(Func<Comment, bool> filter = null)
        {
            return new OperationResult<IList<Comment>>() { Result = this.repository.Get(filter) };
        }

        public OperationResult<Comment> GetFirst(Func<Comment, bool> filter)
        {
            return new OperationResult<Comment>() { Result = this.repository.GetFirst(filter) };
        }

        public OperationResult Remove(string userName, Comment comment)
        {
            return this.Remove(userName, comment.Id);
        }

        public OperationResult Remove(string userName, Guid id)
        {
            var searchCommentResult = this.repository.GetFirst(c => c.Id == id);
            if (searchCommentResult == null)
            {
                return new OperationResult() { Message = "Item not found", Code = ErrorCode.NotFound };
            }

            if (userName != searchCommentResult.UserName)
            {
                return new OperationResult() { Message = "Forbidden", Code = ErrorCode.Forbidden };
            }

            if (!this.repository.Remove(searchCommentResult))
            {
                return new OperationResult() { Message = "Could not delete comment", Code = ErrorCode.Other };
            }

            if (!this.repository.SaveChanges())
            {
                return new OperationResult() { Message = "Could not delete comment", Code = ErrorCode.Other};
            }

            return new OperationResult();
        }

        public OperationResult<Comment> Update(string userName, Guid commentId, string message)
        {
            Comment newComment = new Comment(commentId, userName, message);
            var validationIdResult = this.validator.Validate(newComment, ruleSet: "id");
            var validationMsgResult = this.validator.Validate(newComment, ruleSet: "message");
            if (!validationIdResult.IsValid || !validationMsgResult.IsValid)
            {
                return new OperationResult<Comment>() { Message = "Comment is not valid", Code = ErrorCode.ItemIsNotValid };
            }

            var searchCommentResult = this.repository.GetFirst(c => c.Id == commentId);
            if (searchCommentResult == null)
            {
                return new OperationResult<Comment>() { Message = "Item not found", Code = ErrorCode.NotFound };
            }

            if (userName != searchCommentResult.UserName)
            {
                return new OperationResult<Comment>() { Message = "Forbidden", Code = ErrorCode.Forbidden };
            }

            searchCommentResult.Message = newComment.Message;

            if (!this.repository.SaveChanges())
            {
                return new OperationResult<Comment>() { Message = "Could not delete comment", Code = ErrorCode.Other };
            }

            return new OperationResult<Comment>() { Result = searchCommentResult };
        }
    }
}