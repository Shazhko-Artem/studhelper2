﻿namespace Game_MVC.Infrastructure.Business.Validators
{
    using FluentValidation;
    using Game_MVC.Domain.Core;

    public class CommentValidator : AbstractValidator<Comment>
    {
        public CommentValidator()
        {
            this.RuleSet(
                "id",
                () =>
                    this.RuleFor(comment => comment.Id).NotNull());
            this.RuleSet(
                "name",
                () =>
                    this.RuleFor(comment => comment.UserName).NotNull().NotEmpty());
            this.RuleSet(
                "message",
                () =>
                    this.RuleFor(comment => comment.Message).NotNull().NotEmpty().MinimumLength(1));
        }
    }
}