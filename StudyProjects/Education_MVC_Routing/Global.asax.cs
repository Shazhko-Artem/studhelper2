﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Education_MVC_Routing
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               null, // Route name
               "category/{categoryName}/subcategorie", // URL with parameters
               new { controller = "Categories", action = "SubCategories" } // Parameter defaults
           );

            routes.MapRoute(
               null, // Route name
               "category/{categoryName}/{subCategoryName}", // URL with parameters
               new { controller = "Categories", action = "SubCategory" } // Parameter defaults
           );


            routes.MapRoute(
                null, // Route name
                "category/{categoryName}", // URL with parameters
                new { controller = "Categories", action = "Category" } // Parameter defaults
            );

            routes.MapRoute(
                null,
                "categories",
                new { controller = "Categories", action = "List" }
            );

            routes.MapRoute(
                null,
                "logon",
                new { controller = "Account", action = "LogOn" }
            );

            routes.MapRoute(
                null,
                "logoff",
                new { controller = "Account", action = "LogOff" }
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}