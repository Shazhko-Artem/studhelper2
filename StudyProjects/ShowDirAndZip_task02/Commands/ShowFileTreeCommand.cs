﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02.Commands
{
    public class ShowFileTreeCommand : ICommand
    {
        public int Number => 1;

        public string DisplayName => "Show file tree";

        public void Execute()
        {
            string path = GetDirectoryPath();
            ConsoleColor consoleColor= Console.ForegroundColor;
            ShowDirectoryItems(Directory.CreateDirectory(path));
            Console.ForegroundColor = consoleColor;
        }

        private string GetDirectoryPath()
        {
            Console.Write("[directory path] -> ");
            string path = string.Empty;

            while (string.IsNullOrEmpty(path))
            {
                path = Console.ReadLine();
                if (!Directory.Exists(path))
                {
                    Console.WriteLine("Directory is not exists");
                    path = string.Empty;
                }
            }

            return path;
        }

        private void ShowDirectoryItems(DirectoryInfo directory, int offset=0)
        {
            ShowDirectoryName(directory, offset);

            foreach(var item in directory.GetFiles())
            {
                ShowFileName(item, offset+1);
            }

            foreach (var item in directory.GetDirectories())
            {
                if (item.Attributes.HasFlag(FileAttributes.Directory))
                {
                    ShowDirectoryItems(item, offset + 1);
                }
            }
        }

        private void ShowDirectoryName(DirectoryInfo directory, int offset = 0)
        {
            string itemOffsetIndicator = new string('-', offset * 2);

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"{itemOffsetIndicator} {directory.Name}");
        }

        private void ShowFileName(FileInfo file, int offset = 0)
        {
            string itemOffsetIndicator = new string('-', offset * 2);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{itemOffsetIndicator} {file.Name}");
        }
    }
}
