﻿var homePage = new function () {
    this.onGetUserButtonClick = function (withError) {
        userService.getUsers(withError, function (status, data) {
            if (status == "success") {
                showUser(data);
            } else {
                alert("Error")
            }
        });
    }

    function showUser(user) {
        $("#id").text(user.Id);
        $("#name").text(user.Name);
        $("#age").text(user.Age);
        if (user.Sex) {
            $("#female").attr('checked', 'checked');
        } else {
            $("#male").attr('checked', 'checked');
        }
    }
}