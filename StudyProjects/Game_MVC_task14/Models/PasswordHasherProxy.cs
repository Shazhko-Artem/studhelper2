﻿using Microsoft.AspNet.Identity;

namespace Game_MVC_task14.Models
{
    public class PasswordHasherProxy : Game_MVC.Infrastructure.Business.IPasswordHasher
    {
        private readonly PasswordHasher passwordHasher;

        public PasswordHasherProxy()
        {
            this.passwordHasher = new PasswordHasher();
        }

        public string HashPassword(string password)
        {
            return this.passwordHasher.HashPassword(password);
        }

        public bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return this.passwordHasher.VerifyHashedPassword(hashedPassword, providedPassword) == PasswordVerificationResult.Success;
        }
    }
}