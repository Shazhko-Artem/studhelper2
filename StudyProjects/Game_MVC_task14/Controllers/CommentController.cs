﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Game_MVC.Services.Interfaces;
using Game_MVC_task14.Hubs;
using Microsoft.AspNet.SignalR;

namespace Game_MVC_task14.Controllers
{
    [RoutePrefix("api/Comment")]
    [System.Web.Http.Authorize]
    public class CommentController : ApiController
    {
        private readonly ICommentService commentService;
        private readonly IHubContext notificationHubContext;

        public CommentController(ICommentService commentService)
        {
            this.commentService = commentService;
            this.notificationHubContext = GlobalHost.ConnectionManager.GetHubContext<CommentNotificationHub>();
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var operationResult = this.commentService.Get();
            if (operationResult.State.IsFail())
            {
                switch (operationResult.Code)
                {
                    case ErrorCode.NotFound:
                        return this.NotFound();
                    case ErrorCode.Other:
                        return this.InternalServerError();
                }
            }

            return this.Json(operationResult.Result);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(Guid id)
        {
            var operationResult = this.commentService.GetFirst(item => item.Id == id);
            if (operationResult.State.IsFail())
            {
                switch (operationResult.Code)
                {
                    case ErrorCode.NotFound:
                        return this.NotFound();
                    case ErrorCode.Other:
                        return this.InternalServerError();
                }
            }

            return this.Json(operationResult.Result);
        }

        [HttpPut]
        [Route("{message}")]
        public HttpResponseMessage Add(string message)
        {
            string currentUserName = this.RequestContext.Principal.Identity.Name;
            var operationResult = this.commentService.Add(currentUserName, message);
            if (operationResult.State.IsFail())
            {
                switch (operationResult.Code)
                {
                    case ErrorCode.ItemIsNotValid:
                        return this.Request.CreateResponse(HttpStatusCode.BadRequest);
                    case ErrorCode.Other:
                        return this.Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }

            this.notificationHubContext.Clients.All.onNewComment(operationResult.Result);
            return this.Request.CreateResponse(HttpStatusCode.Created, operationResult.Result);
        }

        [HttpPost]
        [Route("{id}/{message}")]
        public HttpResponseMessage Update(Guid id, string message)
        {
            string currentUserName = this.RequestContext.Principal.Identity.Name;

            var operationResult = this.commentService.Update(currentUserName, id, message);
            if (operationResult.State.IsFail())
            {
                switch (operationResult.Code)
                {
                    case ErrorCode.ItemIsNotValid:
                        return this.Request.CreateResponse(HttpStatusCode.BadRequest);
                    case ErrorCode.NotFound:
                        return this.Request.CreateResponse(HttpStatusCode.NotFound);
                    case ErrorCode.Forbidden:
                        return this.Request.CreateResponse(HttpStatusCode.Forbidden);
                    case ErrorCode.Other:
                        return this.Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }

            this.notificationHubContext.Clients.All.onUpdateComment(operationResult.Result);
            return this.Request.CreateResponse(HttpStatusCode.OK, operationResult.Result);
        }

        [HttpDelete]
        [Route("{id}")]
        public HttpResponseMessage Remove(Guid id)
        {
            string currentUserName = this.RequestContext.Principal.Identity.Name;

            var operationResult = this.commentService.Remove(currentUserName, id);
            if (operationResult.State.IsFail())
            {
                switch (operationResult.Code)
                {
                    case ErrorCode.ItemIsNotValid:
                        return this.Request.CreateResponse(HttpStatusCode.BadRequest);
                    case ErrorCode.NotFound:
                        return this.Request.CreateResponse(HttpStatusCode.NotFound);
                    case ErrorCode.Forbidden:
                        return this.Request.CreateResponse(HttpStatusCode.Forbidden);
                    case ErrorCode.Other:
                        return this.Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }

            this.notificationHubContext.Clients.All.onRemoveComment(id);

            return this.Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}