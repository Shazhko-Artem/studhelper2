﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Game_MVC.Services.Interfaces;
using Game_MVC_task14.ViewModels;
using Microsoft.AspNet.Identity;

namespace Game_MVC_task14.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return this.View(new LoginViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = this.userService.Find(model.UserName, model.Password);
            if (result.State.IsFail())
            {
                this.ModelState.AddModelError(string.Empty, "Incorrect login or password");
                return this.View(model);
            }

            var foundUser = result.Result;
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, foundUser.Name));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, foundUser.Id.ToString()));
            claims.Add(new Claim("Id", foundUser.Id.ToString()));
            var id = new ClaimsIdentity(
                claims,
                DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = this.Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(id);

            return this.Redirect("/");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return this.View(new RegisterUserViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterUserViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var newUser = new Game_MVC.Domain.Core.User()
            {
                Name = model.Name,
                Password = model.Password
            };

            var result = this.userService.Add(newUser);
            if (result.State.IsFail())
            {
                if (result.Code == ErrorCode.AlreadyExist)
                {
                    this.ModelState.AddModelError(string.Empty, "User with the same name already exists");
                    return this.View(model);
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "Something went wrong");
                    return this.View(model);
                }
            }

            var foundUser = result.Result;
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, foundUser.Name));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, foundUser.Id.ToString()));
            var id = new ClaimsIdentity(
                claims,
                DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = this.Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(id);

            return this.Redirect("/");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Logout()
        {
            var authenticationManager = this.Request.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            return this.Redirect("/");
        }
    }
}