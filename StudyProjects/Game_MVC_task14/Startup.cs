﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using FluentValidation;
using Game_MVC.Domain.Core;
using Game_MVC.Domain.Interfaces;
using Game_MVC.Infrastructure.Business;
using Game_MVC.Infrastructure.Business.Validators;
using Game_MVC.Infrastructure.Data;
using Game_MVC.Services.Interfaces;
using Game_MVC_task14.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;

[assembly: OwinStartup(typeof(SignalRMvc.Startup))]
namespace SignalRMvc
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            this.InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);

            app.MapSignalR();
        }

        private void InitializeContainer(Container container)
        {
            container.Register<Game_MVC.Infrastructure.Business.IPasswordHasher, PasswordHasherProxy>(Lifestyle.Singleton);
            container.Register<IGameService, GameService>(Lifestyle.Scoped);
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<ICommentService, CommentService>(Lifestyle.Scoped);
            container.Register(typeof(DomainContext), () => new DomainContext("Server=(localdb)\\mssqllocaldb;Database=gameTest;Trusted_Connection=True;"), Lifestyle.Scoped);
            container.Register(
                typeof(IRepository<User>),
                          typeof(SqlRepository<User>),
                Lifestyle.Scoped);
            container.Register(
                typeof(IRepository<Game>),
                typeof(SqlRepository<Game>),
                Lifestyle.Scoped);
            container.Register(
                typeof(IRepository<UserGameSession>),
                typeof(SqlRepository<UserGameSession>),
                Lifestyle.Scoped);
            container.Register(
                typeof(IRepository<Comment>),
                typeof(InMemoryRepository<Comment>),
                Lifestyle.Singleton);
            container.Register(
                typeof(AbstractValidator<Comment>),
                typeof(CommentValidator),
                Lifestyle.Singleton);
        }
    }
}