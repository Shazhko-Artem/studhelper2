﻿$(function () {
    var currentUserName;
    var gameHub = $.connection.gameHub;
    var controllerUrl = "game";

    //gameHub.client.onConnected = function (userName) {
    //    $("#btnSetNumber").removeClass('disabled');
    //    currentUserName = userName;
    //}

    gameHub.client.onUserDisconnected = function (userName) {
        $("#user_" + userName).remove();
    }

    gameHub.client.onRollColl = function (userName) {
        $.ajax({
            type: "POST",
            url: `${controllerUrl}/markYourSelf`,
            dataType: "json"
        });
    }

    gameHub.client.onMarkedOtherUsers = function (userName, gameStatus) {
        if (currentUserName !== userName) {
            if ($("#user_" + userName).length === 0) {
                $("#usersList").append('<li id="user_' + userName + '">' + userName + '</li>');
            }
        }
    }

    //gameHub.client.onCompareNumber = function (data, state) {
    //    $("#msg").html(data);
    //}

    //gameHub.client.onLoss = function (data) {
    //    $("#msg").html(data);
    //}

    //gameHub.client.onWin = function (data) {
    //    $('#compareBlock').hide();
    //    $("#msg").html(data);
    //}

    gameHub.client.onStartGame = function () {
        startGame();
    }

    gameHub.client.onGameOver = function (userName, massage) {
        if (userName !== currentUserName) {
            alert(massage);
            $('#compareBlock').hide();
            $("#msg").html("");
        }
    }
    
    $.connection.hub.start().done(function () {
        $('#setNumberBlock').show();
        $('#btnCompereNumber').click(function () {
            $.ajax({
                type: "POST",
                url: `${controllerUrl}/compare/${$('#inputNumber').val()}`,
                dataType: "json",
                success: function (data) {
                    if (data.result === 'loss') {
                        $("#msg").html(data.message);
                    }
                    else if (data.result === 'win') {
                        $('#compareBlock').hide();
                        $("#msg").html(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
        });

        $("#btnSetNumber").click(function () {
            $.ajax({
                type: "POST",
                url: `${controllerUrl}/set/${$('#inputNewNumber').val()}`,
                dataType: "json"
            });
        });

        $.ajax({
            type: "POST",
            url: `${controllerUrl}/connect`,
            dataType: "json",
            success: function (data) {
                currentUserName = data.userName;
                if (data.gameStatus === 'running') {
                    startGame();
                }

                $.ajax({
                    type: "POST",
                    url: `${controllerUrl}/rollColl`,
                    dataType: "json"
                });
            }    
        });
    });

    $(window).on('beforeunload ', function () {
        console.log("beforeunload");
        $.connection.hub.stop();
    });

    function startGame() {
        $('#compareBlock').show();
        $("#msg").html("");
    }
});