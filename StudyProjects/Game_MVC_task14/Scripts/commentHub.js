﻿$(function () {
    var currentUserName;
    var commentHub = $.connection.commentNotificationHub;
    var controllerUrl = "api/Comment";

    commentHub.client.onConnected = function (userName) {
        currentUserName = userName;
    }

    commentHub.client.onNewComment = function (newComment) {
        addComment(newComment);
    };

    commentHub.client.onUpdateComment = function (comment) {
        $('#' + comment.Id).html(buildCommentBlock(comment));
    };

    commentHub.client.onRemoveComment = function (commentId) {
        $('#' + commentId).remove();
    };

    $.connection.hub.start().done(function () {
        $('#btnSendComment').click(function () {
            $.ajax({
                type: "PUT",
                url: `${controllerUrl}/${$('#inputComment').val()}`,
                dataType: "json"
            });
        });

        commentHub.server.connect();

        loadComments();
    });

    function buildCommentBlock(comment) {
        var commentBlock = $(`<b>${comment.UserName}: </b>`)
            .add(`<span>${comment.Message}</span>`);
        if (comment.UserName === currentUserName) {
            var controlButtons = $('<div class="btn-group control-btn-group"></div>');
            controlButtons
                .append($('<input class="btn btn-default btn-editComment" type="button" value="edit"/>').click(onBtnEditCommentClick))
                .append($('<input class="btn btn-default btn-saveComment radius-left hidden" type = "button" value = "save" />').click(onBtnSaveCommentClick))
                .append($('<input class="btn btn-default btn-cancelComment hidden" type = "button" value = "cancel" />').click(onBtnCancelCommentClick))
                .append($('<input class="btn btn-default btn-deleteComment" type = "button" value = "delete"/>').click(onBtnDeleteCommentClick));
            commentBlock = commentBlock.add(controlButtons);
        }
        return commentBlock;
    }

    function loadComments() {
        $.ajax({
            type: "GET",
            url: controllerUrl,
            success: function (data, textStatus) {
                data.forEach(function (comment) {
                    addComment(comment);
                });
            },
            dataType: "json"
        });
    }

    function addComment(comment) {
        $('#commentsList')
            .append($(`<li id="${comment.Id}" class="list-group-item"/>`)
                .append(buildCommentBlock(comment)));
    }

    function onBtnDeleteCommentClick() {
        var parent = $(this).parent().parent();
        $.ajax({
            type: "DELETE",
            url: `${controllerUrl}/${parent.attr('id')}`,
            dataType: "json"
        });
    }

    function onBtnEditCommentClick() {
        var parent = $(this).parent().parent();
        parent.children('span').replaceWith($('<textarea/>').append(parent.children('span').html()));
        var parent = $(this).parent();
        parent.children('.btn-editComment').addClass('hidden');
        parent.children('.btn-cancelComment').removeClass('hidden');
        parent.children('.btn-saveComment').removeClass('hidden');
    }

    function onBtnSaveCommentClick() {
        var parent = $(this).parent().parent();
        $.ajax({
            type: "POST",
            url: `${controllerUrl}/${parent.attr('id')}/${parent.children('textarea').val()}`,
            dataType: "json"
        });
    }

    function onBtnCancelCommentClick() {
           var parent = $(this).parent().parent();
        parent.children('textarea').replaceWith($('<span/>').append(parent.children('textarea').html()));
        var parent = $(this).parent();
        parent.children('.btn-editComment').removeClass('hidden');
        parent.children('.btn-cancelComment').addClass('hidden');
        parent.children('.btn-saveComment').addClass('hidden');
    }
});