﻿namespace Game_MVC.Services.Interfaces
{
    public enum ErrorCode
    {
       None = 0,
       Forbidden = 100,
       ItemIsNotValid = 200,
       NotFound = 201,
       AlreadyExist = 202,
       Other = 300
    }
}