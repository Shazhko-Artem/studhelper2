﻿using System;

namespace Game_MVC.Services.Interfaces.Projections
{
    public class UserProjection
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}