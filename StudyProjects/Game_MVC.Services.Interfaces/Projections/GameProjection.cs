﻿using System;
using System.Collections.Generic;

namespace Game_MVC.Services.Interfaces.Projections
{
    public class GameProjection
    {
        public Guid Id { get; set; }

        public UserProjection Owner { get; set; }

        public int Number { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public UserProjection Winner { get; set; }

        public IList<UserGameSessionProjection> GameSessions { get; set; } = new List<UserGameSessionProjection>();
    }
}