﻿namespace Game_MVC.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Game_MVC.Domain.Core;
    using Game_MVC.Services.Interfaces.Projections;

    public interface IGameService
    {
        bool IsRunning { get; }

        OperationResult SetNumber(UserProjection user, int number);

        OperationResult<int> CompareNumber(UserProjection user, int number);

        OperationResult<IList<GameProjection>> GetGameHistory(Func<Game, bool> filter = null);

        OperationResult<IList<UserGameSessionProjection>> GetGameDetails(GameProjection game);

        OperationResult<IList<UserGameSessionProjection>> GetGameDetails(Guid gameId);
    }
}