﻿namespace Game_MVC.Services.Interfaces
{
    public class OperationResult
    {
        public string Message { get; set; }

        public ResultState State => this.Code == ErrorCode.None ? ResultState.Success : ResultState.Fail;

        public ErrorCode Code { get; set; }
    }
}