﻿namespace Game_MVC.Services.Interfaces
{
    public static class OperationResultExtension
    {
        public static bool IsSuccess(this ResultState resultState)
        {
            return resultState == ResultState.Success;
        }

        public static bool IsFail(this ResultState resultState)
        {
            return resultState == ResultState.Fail;
        }
    }
}