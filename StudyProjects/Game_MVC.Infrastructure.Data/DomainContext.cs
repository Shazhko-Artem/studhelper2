﻿namespace Game_MVC.Infrastructure.Data
{
    using Game_MVC.Domain.Core;
    using Microsoft.EntityFrameworkCore;

    public class DomainContext : DbContext
    {
        private readonly string connectionString;

        public DomainContext(string connectionString)
        {
            this.connectionString = connectionString;
            this.Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<UserGameSession> UserGameSessions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasAlternateKey(e => e.Name);

                entity.HasMany(e => e.GameSessions).WithOne(gs => gs.User).HasForeignKey(gs => gs.UserId);

                entity.HasMany(e => e.Owners).WithOne(gs => gs.Owner).HasForeignKey(gs => gs.OwnerId).OnDelete(DeleteBehavior.Restrict);

                entity.HasMany(e => e.WonGames).WithOne(gs => gs.Winner).HasForeignKey(gs => gs.WinnerId).OnDelete(DeleteBehavior.SetNull).IsRequired(false);
            });

            modelBuilder.Entity<Game>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasMany(e => e.GameSessions).WithOne(gs => gs.Game).HasForeignKey(gs => gs.GameId).OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<UserGameSession>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasOne(e => e.Game).WithMany(gs => gs.GameSessions).HasForeignKey(gs => gs.GameId).OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.User).WithMany(gs => gs.GameSessions).HasForeignKey(gs => gs.UserId).OnDelete(DeleteBehavior.Cascade);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(this.connectionString);
        }
    }
}