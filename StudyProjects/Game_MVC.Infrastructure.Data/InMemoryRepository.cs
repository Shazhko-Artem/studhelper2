﻿namespace Game_MVC.Infrastructure.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Game_MVC.Domain.Core;
    using Game_MVC.Domain.Interfaces;

    public class InMemoryRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IUnique
    {
        private readonly List<TEntity> entities;

        public InMemoryRepository()
        {
            this.entities = new List<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            this.entities.Add(entity);
            entity.Id = Guid.NewGuid();
            return entity;
        }

        public IList<TEntity> Get(Func<TEntity, bool> filter = null, string includeProperties = "")
        {
            if (filter == null)
            {
                return this.entities.ToList();
            }

            return this.entities.Where(filter)?.ToList();
        }

        public TEntity GetFirst(Func<TEntity, bool> filter, string includeProperties = "")
        {
            if (filter == null)
            {
                return this.entities.FirstOrDefault();
            }

            return this.entities.FirstOrDefault(filter);
        }

        public TEntity GetLast(Func<TEntity, bool> filter = null, string includeProperties = "")
        {
            if (filter == null)
            {
                return this.entities.LastOrDefault();
            }

            return this.entities.LastOrDefault(filter);
        }

        public bool Remove(TEntity entity)
        {
            return this.entities.Remove(entity);
        }

        public TEntity Update(TEntity entity)
        {
            var foundEntity = this.entities.FirstOrDefault(e => e.Id == entity.Id);
            if (foundEntity == null)
            {
                return null;
            }
            else
            {
                this.entities.Remove(foundEntity);
                this.entities.Add(entity);
            }

            return entity;
        }

        public bool SaveChanges()
        {
            return true;
        }
    }
}