﻿namespace Game_MVC.Infrastructure.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Game_MVC.Domain.Interfaces;
    using Microsoft.EntityFrameworkCore;

    public class SqlRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly DomainContext context;
        private readonly DbSet<TEntity> dbSet;

        public SqlRepository(DomainContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
        }

        internal DomainContext Context => this.context;

        internal DbSet<TEntity> DbSet => this.dbSet;

        public virtual TEntity Add(TEntity entity)
        {
          return this.dbSet.Add(entity).Entity;
        }

        public virtual IList<TEntity> Get(Func<TEntity, bool> filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbSet;
            if (filter != null)
            {
                query = query.Where(item => filter(item));
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.ToList();
        }

        public virtual TEntity GetFirst(Func<TEntity, bool> filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbSet;

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (filter != null)
            {
                return query.FirstOrDefault((e) => filter(e));
            }

            return query.FirstOrDefault();
        }

        public virtual TEntity GetLast(Func<TEntity, bool> filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbSet;

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (filter != null)
            {
                return query.FirstOrDefault((e) => filter(e));
            }

            return query.LastOrDefault();
        }

        public virtual bool Remove(TEntity entity)
        {
            this.context.Remove(entity);
            return true;
        }

        public TEntity Update(TEntity entity)
        {
            return this.context.Update(entity).Entity;
        }

        public virtual bool SaveChanges()
        {
            try
            {
                this.context.SaveChanges();
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[{DateTime.Now}] Exception: {exception.Message}{Environment.NewLine}When save changes in the sqlRepository");
                return false;
            }

            return true;
        }
    }
}