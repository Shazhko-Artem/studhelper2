﻿using System;

namespace Game_MVC.Domain.Core
{
    public interface IUnique
    {
        Guid Id { get; set; }
    }
}