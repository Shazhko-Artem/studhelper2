﻿namespace Game_MVC.Domain.Core
{
    using System;

    public class Comment : IUnique
    {
        public Comment()
        {
        }

        public Comment(string userName)
        {
            this.UserName = userName;
        }

        public Comment(string userName, string message)
            : this(userName)
        {
            this.Message = message;
        }

        public Comment(Guid id, string userName, string message)
            : this(userName, message)
        {
            this.Id = id;
        }

        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Message { get; set; }
    }
}