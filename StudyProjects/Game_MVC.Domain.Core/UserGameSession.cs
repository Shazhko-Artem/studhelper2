﻿using System;

namespace Game_MVC.Domain.Core
{
    public class UserGameSession : IUnique
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }

        public Game Game { get; set; }

        public Guid GameId { get; set; }

        public int Number { get; set; }

        public DateTime DateTime { get; set; }
    }
}